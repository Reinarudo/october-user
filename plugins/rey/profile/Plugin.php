<?php namespace Rey\Profile;

use Backend;
use System\Classes\PluginBase;
use RainLab\User\Models\User as UserModel;
use RainLab\User\Controllers\Users as UsersController;
use Rey\Profile\Models\Profile as ProfileModel;

/**
 * Profile Plugin Information File
 */
class Plugin extends PluginBase
{
    //this will create a dependency to the Rainlab.User plugin so it ensures that its table
    //is migrated first before this and the execution of methods are done in the correct order - rey
    public $require = ['RainLab.User']; 

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Profile',
            'description' => 'No description provided yet...',
            'author'      => 'Rey',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

        UserModel::extend(function($model) {
            $model->belongsTo['department'] = [ \Rey\Departments\Models\Department::class ];
        });
        
        
        UsersController::extendListColumns(function($list, $model) {
            if (!$model instanceof UserModel)
                return;

            $list->addColumns([
                'headline' => [
                    'label' => 'Headline'
                ]
            ]);
        });

        //extendFormFields is defined in the backend form documentation
        //basically tells, "anytime a form is rendered by this controller, give us the opportunity to provide extra fields"
        UsersController::extendFormFields(function($form, $model, $context){
            
            //we need this so that when somebody else adds a form to this controller, 
            //we don't want to pollute it with this fields we've extended
            if (!$model instanceof UserModel)
                return;
            
            //note that we use profile[fieldname] as it indicates that the record is for the other table (which is profile relationship key)
            $form->addTabFields([
                'department' => [
                    'label' => 'Department',
                    'tab' => 'Profile',
                    'type' => 'recordfinder',
                    'recordsPerPage' =>  '10',
                    'title' =>  'Find Record',
                    'prompt' =>  'Click the Find button to find a department',
                    'keyFrom' =>  'id',
                    'nameFrom' =>  'name',
                    'modelClass' => \Rey\Departments\Models\Department::class,
                    'list' => '~/plugins/rey/profile/models/department/columns.yaml'
                ],
                'headline' => [
                    'label' => 'Headline',
                    'tab' => 'Profile',
                    'type' => 'textarea'
                ],
                'about_me' => [
                    'label' => 'About Me',
                    'tab' => 'Profile',
                    'type' => 'textarea'
                ],
                'interests' => [
                    'label' => 'Interests',
                    'tab' => 'Profile',
                    'type' => 'textarea'
                ],
                'books' => [
                    'label' => 'Books',
                    'tab' => 'Profile',
                    'type' => 'textarea'
                ],
                'music' => [
                    'label' => 'Music',
                    'tab' => 'Profile',
                    'type' => 'textarea'
                ],
            ]);
        });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Rey\Profile\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'rey.profile.some_permission' => [
                'tab' => 'Profile',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'profile' => [
                'label'       => 'Profile',
                'url'         => Backend::url('rey/profile/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['rey.profile.*'],
                'order'       => 500,
            ],
        ];
    }
}
