<?php namespace Rey\Profile\Models;

use Model;

/**
 * Model
 */
class Department extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'rey_profile_departments';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $hasMany = [
        'users' => \RainLab\User\Controllers\Users::class
    ]; 
}
