<?php namespace Rey\Departments\Models;

use Model;

/**
 * Model
 */
class Department extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\SimpleTree;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'rey_departments_departments';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
